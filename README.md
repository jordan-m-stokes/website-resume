# website-resume | version-1

## setup 
- clone project
- install npm and nodejs if not installed:   https://nodejs.org/en/download/
- run `npm install` in project directory to install dependencies

## usage
* run `npm start` in project directory to run server

## development
* run `npm run dev` to run server and watch for changes