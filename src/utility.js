import React from 'react';
import ReactHtmlParser from 'react-html-parser';

import Highlight, {openingTag, closingTag} from './app/components/custom-elements/Highlight';

// returns a component that serves as a highlight in the text of a p, h1, h2, etc...
export function highlight(text = "")
{
    return <Highlight text={text} />;
}

// marks in the text to look for and replace as highlight elements
const openingMark = '[h]';
const closingMark = '[h/]';

// takes a string with marking indicating a need for a highlight and highlights them ( #h#text#/h# -> <span class="highlight">text</span> )
// then returns the text as jsx
export function highlightMarkedText(text = "")
{
    while(text.includes(openingMark))
    {
        text = text.replace(openingMark, openingTag);
    }
    while(text.includes(closingMark))
    {
        text = text.replace(closingMark, closingTag);
    }

    return ReactHtmlParser(text);
}

// removes extra spaces from class name
export function cleanClassName(element)
{
    while(element.className.includes('  '))
    {
        element.className = element.className.replace('  ', ' ');
    }
}

// takes an element and a class name and toggles the class name on or off on the element
export function toggleClassName(element, className)
{
    if(element.className.includes(className))
    {
        element.className = element.className.replace(className, '');
    }
    else
    {
        element.className += ' ' + className;
    }
    cleanClassName(element);
}

// adds or removes the given className based on the provided bool
export function setClassName(element, className, enable)
{
    if(enable && !element.className.includes(className))
    {
        element.className += ' ' + className;
    }
    else if(!enable && element.className.includes(className))
    {
        element.className = element.className.replace(className, '');
    }
    cleanClassName(element);
}

export function hasClassName(element, className)
{
    return element.className.includes(className);
}