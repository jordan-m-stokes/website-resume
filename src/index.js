import React from 'react';
import ReactDOM from 'react-dom';
import './styles/normalize.css';
import App from './app/App';
import { Provider } from './app/components/context';

ReactDOM.render(
  <Provider>
    <App />
  </Provider>,
  document.getElementById('root')
);
