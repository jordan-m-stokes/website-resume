import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Consumer } from './components/context';

// styles
import '../styles/App.scss';

// routes
import Index from './routes/Index';
import Projects from './routes/Projects';
import Experience from './routes/Experience';
import CurrentWork from './routes/CurrentWork';
import Education from './routes/Education';
import Contact from './routes/Contact';
import Forward from './routes/Forward';

// components
import Header from './components/Header';
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import ProjectModal from './components/ProjectModal';

export default class App extends Component
{
	// handles the user scrolling the window
	scrollHandler = () =>
	{
		let halfViewportHeight, middleExpanse, indicatorPosition, indicatorDistanceFromMiddle, normalizedDistance, indicatorOpacity, indicatorFontSize;

		halfViewportHeight = window.innerHeight / 2;
		middleExpanse = halfViewportHeight * 0.225;

		// loops through all scroll-based indicators and changes their visibility based on how
		// close to the center of the viewport they are
		for(let indicator of this.scrollBasedIndicators)
		{
			indicatorPosition = indicator.getBoundingClientRect().top;
			indicatorDistanceFromMiddle = indicatorPosition - halfViewportHeight;
			indicatorDistanceFromMiddle = Math.abs(indicatorDistanceFromMiddle);
			indicatorDistanceFromMiddle = (indicatorDistanceFromMiddle <= middleExpanse) ? (0) : indicatorDistanceFromMiddle - middleExpanse; 

			if(indicatorDistanceFromMiddle > halfViewportHeight)
			{
				continue;
			}

			normalizedDistance = indicatorDistanceFromMiddle / halfViewportHeight;
			normalizedDistance = (normalizedDistance > 1) ? (1) : (normalizedDistance);

			indicatorOpacity = (1 - normalizedDistance - 0.5) * 2;
			indicatorOpacity = (indicatorOpacity < 0.1) ? (0.1) : (indicatorOpacity);
			indicatorFontSize = (1 - normalizedDistance - 0.5) * 2;
			indicatorFontSize = (indicatorFontSize < 0) ? (0) : (indicatorFontSize);
			indicatorFontSize = 13 + (5 * indicatorFontSize);

			indicator.style.opacity = `${indicatorOpacity}`;
			indicator.style.fontSize = `${indicatorFontSize}px`;
		}
	}

	componentDidMount()
	{
		// stores element references that handlers continually use (performance reasons)
		this.scrollBasedIndicators = document.querySelectorAll('.scroll-based-indicator');

		// attatches scroll handler to the document

		window.addEventListener('scroll', this.scrollHandler);
	}

	render()
	{
		return (
			<div className="App">
				<Router>
					<Consumer>
					{ ({ actions }) => 
					(
						<div id="main-flow" onClick={actions.handleProjectModalCloseRequest}>
							{/* top content for all routes */}
							<Route path="/" component={Header} />
							{/* exact route content */}
							<Route exact path="/" component={Index} />
							<Route exact path="/projects" component={Projects} />
							<Route exact path="/experience" component={Experience} />
							<Route exact path="/current-work" component={CurrentWork} />
							<Route exact path="/education" component={Education} />
							<Route exact path="/contact" component={Contact} />
							{/* utility route for creating a pdf to upload as a resume forwarding to this site */}
							{/* <Route exact path="/forward" component={Forward} /> */}
							{/* bottom content for all routes */}
							<Route path="/" component={Navigation} />
							<Route path="/" component={Footer} />
						</div>
					)}
					</Consumer>
					{/* fixed content for all routes */}
					<Route path="/" component={ProjectModal} />
				</Router>
			</div>
		);
	}
}