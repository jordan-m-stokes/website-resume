import React, { Component } from 'react';

// styles
import '../../styles/routes/Contact.scss';

// utility functions
import { highlight } from '../../utility';

export default class Contact extends Component
{
    componentDidMount()
    {
        let contactLink = document.querySelector('#link-contact');

        contactLink.style.color = 'gold';
    }

    render() 
    {
        return (
            <section id="contact">
                <h2>
                    {highlight('c')}ontac{highlight('t')}
                </h2>
                <div className="method-of-contact">
                    <a rel="noopener noreferrer" href="mailto:jordan.m.stokes@outlook.com" className="contact-link">
                        <img src="./resources/mail.png" alt="" className="contact-link-icon"/>
                    </a>
                    <span className="contact-info">jordan.m.stokes@outlook.com</span>
                </div>
                <div className="method-of-contact">
                    <a rel="noopener noreferrer" href="tel:512-638-5754" className="contact-link">
                        <img src="./resources/phone.png" alt="" className="contact-link-icon"/>
                    </a>
                    <span className="contact-info">(512) 638-5754</span>
                </div>
                <div className="method-of-contact">
                    <a  rel="noopener noreferrer" href="https://www.google.com/maps/place/Cedar+Park,+TX/" target="_blank" className="contact-link">
                        <img src="./resources/location.png" alt="" className="contact-link-icon"/>
                    </a>
                    <span className="contact-info">Cedar Park, TX 78613</span>
                </div>
            </section>

        );
    }
}