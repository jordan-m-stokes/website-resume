import React, { Component } from 'react';

// styles
import '../../styles/routes/Experience.scss';

// utility functions
import { highlight } from '../../utility';


export default class Experience extends Component
{
    componentDidMount()
    {
        let experienceLink = document.querySelector('#link-experience');

        experienceLink.style.color = 'gold';
    }

    render() 
    {
        return (
			<section id="experience">
				<h2>
					{highlight('a')}bilitie{highlight('s')}
				</h2>
				<div id="abilities">
					<ul className="points">
						<li>
							{highlight('Leadership')} and {highlight('team-building ')} 
							skills from managing a {highlight('Malcom Baldrige')} award-winning company.
						</li>
						<li>
							{highlight('7 years')} of experience developing software
						</li>
						<li>Application of conventional coding practices in the form of {highlight('commenting')}
							, {highlight('testing')}, and maintaining clean {highlight('DRY code')}
						</li>
					</ul>
				</div>
					<h2>
						{highlight('k')}nowledg{highlight('e')}
					</h2>
					<div id="knowledge">
						<ul className="color-list">
							<h4>
								{highlight('k')}nown{highlight('-l')}anguage{highlight('s')}
							</h4>
							<li style={{background: "#958c23"}}>javascript</li>
							<li style={{background: "#96265c"}}>html</li>
							<li style={{background: "#279b97"}}>css/sass/bootstrap</li>
							<li style={{background: "#926629"}}>java</li>
							<li style={{background: "#90392a"}}>ruby</li>
							<li style={{background: "#2b7092"}}>c#</li>
							<li style={{background: "#2c8f55"}}>python</li>
						</ul>
						<ul className="color-list">
							<h4>
								{highlight('p')}racticed{highlight('-f')}ramework{highlight('s')}
							</h4>
							<li style={{background: "#279b97"}}>react</li>
							<li style={{background: "#96265c"}}>express</li>
							<li style={{background: "#2b7092"}}>angular</li>
							<li style={{background: "#90392a"}}>ruby-on-rails</li>
						</ul>
						<ul className="color-list">
							<h4>
								{highlight('d')}atabases{highlight('-w')}orked{highlight('-w')}it{highlight('h')}
							</h4>
							<li style={{background: "#2c8f55"}}>mongodb</li>
							<li style={{background: "#279b97"}}>postgresql</li>
						</ul>
						<ul className="color-list">
							<h4>
								{highlight('c')}ommand{highlight('-l')}ine{highlight('-p')}roficienc{highlight('y')}
							</h4>
							<li style={{background: "#90392a"}}>git</li>
							<li style={{background: "#96265c"}}>npm</li>
							<li style={{background: "#958c23"}}>framework-clis</li>
						</ul>
					</div>
				</section>
		);
    }
}