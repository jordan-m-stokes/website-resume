import React, { Component } from 'react';

// data
import projects from '../../data/projects';

// styles
import '../../styles/routes/Forward.scss';

// utility functions
import { highlight } from '../../utility';
import { constructFeaturedProjects } from './Projects';

export default class Forward extends Component
{
    componentDidMount()
    {
        let aboutLink = document.querySelector('#link-about');

        aboutLink.style.color = 'gold';
    }

    render() 
    {
        return (
			<div className="index">
				<section id="forward">
					<header id="forward-header">
						<div id="forward-photo"></div>
						<h1>
							{highlight('j')}ordan m{highlight('.')} stoke{highlight('s')}
						</h1>
					</header>
					<p id="forward-description">
						<span className="highlight paragraph-start">H</span>ello! This is a forward to my true resume. Everything about me and my work can be found on my portfolio site here:
					</p>
					<a id="forward-link" href="https://www.jordanstokes.me/">jordanstokes.me</a>
				</section>
			</div>
		);
    }
}