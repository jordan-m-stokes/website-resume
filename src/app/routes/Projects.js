import React, { Component } from 'react';

import Project from '../components/Project';

// styles
import '../../styles/routes/Education.scss';

// data
import projects from '../../data/projects';

// utility functions
import { highlight } from '../../utility';

export default class Education extends Component
{
    componentDidMount()
    {
        let educationLink = document.querySelector('#link-projects');

        educationLink.style.color = 'gold';
    }

    render() 
    {
        return (
            <section id="featured-projects">
					<h2>
						{highlight('f')}eatured{highlight('-p')}roject{highlight('s')}
					</h2>
					<ul className="project-list">
						{constructFeaturedProjects(projects)}
					</ul>
				</section>
        );
    }
}

// returns the jsx for all projects marked as featured
export function constructFeaturedProjects(json)
{
	let jsx = [];

	for(let projectId of Object.keys(json))
	{
		let projectData = json[projectId];

		if(projectData.featured)
		{
			jsx.push(<Project id={projectId} data={projectData} key={projectId}/>);
		}
	}
	
	return jsx;
}

