import React, { Component } from 'react';

// styles
import '../../styles/routes/CurrentWork.scss';

// utility functions
import { highlight } from '../../utility';

export default class CurrentWork extends Component
{
    componentDidMount()
    {
        let currentWorkLink = document.querySelector('#link-current-work');

        currentWorkLink.style.color = 'gold';
    }

    render() 
    {
        return (
            <section id="current-work">
                <h2>
                    {highlight('c')}urrent{highlight('-w')}or{highlight('k')}
                </h2>
                <div id="role-shift-leader" className="role">
                    <div className="role-content">
                        <h4>
                            {highlight('s')}hift{highlight('-l')}eade{highlight('r')} | {highlight('k')}&amp;n{highlight('-m')}anagemen{highlight('t')} | {highlight('a')}ustin{highlight('-')}t{highlight('x')} | {highlight('s')}ince{highlight('-2')}01{highlight('6')}
                        </h4>
                        <p>
                            <span className="highlight paragraph-start">A</span> back-of-house {highlight('leadership')} role 
                            at {highlight('Rudy\'s Country Store')} and {highlight('BBQ')}, geared toward inspiring my {highlight('team members')} to be the best, 
                            pitching in and cutting, building orders, prepping, and {highlight('settings an example')} for the {highlight('dedicated')} workers I serve.
                        </p>
                    </div>
                </div>
                <div id="role-contract-work" className="role">
                    <div className="role-content">
                        <h4>
                            {highlight('c')}ontract{highlight('-d')}evelope{highlight('r')} | {highlight('k')}&amp;n{highlight('-m')}anagemen{highlight('t')} | {highlight('a')}ustin{highlight('-')}t{highlight('x')} | {highlight('s')}ince{highlight('-2')}02{highlight('0')}
                        </h4>
                        <p>
                            <span className="highlight paragraph-start">A</span> role where I put my skills as a 
                            {highlight(' developer')} to use, creating {highlight('online tools')} for daily use that 
                            {highlight(' streamline')} what were once tedious, monotonous tasks.
                        </p>
                    </div>
                </div>
            </section>
        );
    }
}