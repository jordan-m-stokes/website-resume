import React, { Component } from 'react';

// styles
import '../../styles/routes/Education.scss';

// utility functions
import { highlight } from '../../utility';

export default class Education extends Component
{
    componentDidMount()
    {
        let educationLink = document.querySelector('#link-education');

        educationLink.style.color = 'gold';
    }

    render() 
    {
        return (
            <section id="education">
                <h2>
                    {highlight('e')}ducatio{highlight('n')}
                </h2>
                <h4>
                    {highlight('t')}echdegre{highlight('e')} | {highlight('f')}ull{highlight('-s')}tack{highlight('-j')}avascrip{highlight('t')} | {highlight('t')}reehous{highlight('e')} | {highlight('o')}ct{highlight('-2')}01{highlight('9')}
                </h4>
                <a id="certification" rel="noopener noreferrer" href="https://www.credential.net/61adfd86-24ac-4cb5-ba0f-d802f3a76204#gs.9971md" target="_blank">
                    <img src="resources/certification.jpg" alt="" />
                </a>
                <p id="certification-pointer">^</p>
                <p id="certification-director">click to view</p>
                <div id="education-other-links">
                    <a id="techedegree-info" rel="noopener noreferrer" href="https://join.teamtreehouse.com/techdegree/" target="_blank">techdegree-info</a>
                    <span>|</span>
                    <a id="treehouse-profile" rel="noopener noreferrer" href="https://teamtreehouse.com/jordanstokes" target="_blank">treehouse-profile</a>
                </div>
            </section>
        );
    }
}