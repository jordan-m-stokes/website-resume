import React, { Component } from 'react';

// data
import projects from '../../data/projects';

// styles
import '../../styles/routes/Index.scss';

// utility functions
import { highlight } from '../../utility';
import { constructFeaturedProjects } from '../routes/Projects';

export default class Index extends Component
{
    componentDidMount()
    {
        let aboutLink = document.querySelector('#link-about');

        aboutLink.style.color = 'gold';
    }

    render() 
    {
        return (
			<div className="index">
				<section id="me">
					<header id="bio-header">
						<div id="profile-photo"></div>
						<h1>
							{highlight('j')}ordan m{highlight('.')} stoke{highlight('s')}
						</h1>
						<h2>
							{highlight('a')}bout{highlight('-')}m{highlight('e')}
						</h2>
					</header>
					<p id="bio">
						<span className="highlight paragraph-start">A</span>t the end of the day, I love what I do. Every time I clock
						in I'm excited to work with the people I work
						with, doing my job, and making it fun. And every time I find a coding project to attack, I carry out the
						objective anticipating the excitement and pride I'd feel when it's done and done well. The commitment,
						passion, and excellence I can find in my work is what I value most.
					</p>
				</section>
				<section id="featured-projects">
					<h2>
						{highlight('f')}eatured{highlight('-p')}roject{highlight('s')}
					</h2>
					<ul className="project-list">
						{constructFeaturedProjects(projects)}
						<a id="link-all-projects" href="/projects">view all projects</a>
					</ul>
				</section>
			</div>
		);
    }
}