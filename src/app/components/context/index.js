import React, { Component } from 'react';

// data
import projects from '../../../data/projects';

// utility functions
import { setClassName } from '../../../utility';

const ScoreboardContext = React.createContext();

export class Provider extends Component
{
    state =
    {
        selectedProjectData: 
        {
            id: "",
			display: "",
			lede: "",
			pointers: [],
			links: 
			{
				liveApp: "",
				repository: "",
			},
			imageCount: 0,
			featured: false,
        },
        projectModalIsHidden: true,
    };

    // handles when a user clicks on a project to view
    handleProjectSelection = (event) => 
    {
        let target = event.target;

        // checks to make sure the target element is indeed the project thumbnail
        if(!target.className === 'project-thumbnail')
        {
            return;
        }

        // gets the id of the parent li project element and sets the data to that of that project
        let projectElement = target.parentNode;
        let id = projectElement.id;
        let newState = 
        {
            selectedProjectData: projects[id],
            projectModalIsHidden: false,
        };

        newState.selectedProjectData.id = id;

		this.setState(prevState => newState);

        // gets and exposes project modal, blurs the main flow, and hides navigation
        let projectModal = document.querySelector('#project-modal');
        let mainFlow = document.querySelector('#main-flow');
        let navigation = document.querySelector('#navigation');

        setClassName(projectModal, 'hidden', false);
        setClassName(mainFlow, 'blurred', true);
        setClassName(navigation, 'hidden', true);
    }

    // handles when the user tries to close the project modal
    handleProjectModalCloseRequest = () =>
    {
        // if the state of the project modal is already hidden, this function returns
        if(this.state.projectModalIsHidden)
        {
            return;
        }

        // if not the project modal state is set as not hidden
        let newState = 
        {
            selectedProjectData: 
            {
                id: "",
                display: "",
                lede: "",
                pointers: [],
                links: 
                {
                    liveApp: "",
                    repository: "",
                },
                imageCount: 0,
                featured: false,
            },
            projectModalIsHidden: true,
        };

        this.setState(prevState => newState);

        // marks the modal as hidden and the main flow as unblurred so the css can do its work
        let modal = document.querySelector('#project-modal');
        let modalContent = modal.querySelector('.modal-content');
        let mainFlow = document.querySelector('#main-flow')
        let navigation = document.querySelector('#navigation');

        setClassName(modal, 'hidden', true);
        setClassName(mainFlow, 'blurred', false);

        // waits for the animation to finish and reexposes navigation
        setTimeout(() => setClassName(navigation, 'hidden', false), 150);

        // resets scroll so the next selected project is displayed starting at the top
        modalContent.scrollTo(0, 0);
    }

    handleProjectImageCloseRequest = () =>
    {

    }

    render()
    {
        return(
            <ScoreboardContext.Provider value=
            {{
                selectedProjectData: this.state.selectedProjectData,
                actions: {
                    handleProjectSelection: this.handleProjectSelection,
                    handleProjectModalCloseRequest: this.handleProjectModalCloseRequest,
                    handleProjectImageCloseRequest: this.handleProjectImageCloseRequest,

                }
            }}>

            { this.props.children }

            </ScoreboardContext.Provider>
        );
    }
};
export const Consumer = ScoreboardContext.Consumer;