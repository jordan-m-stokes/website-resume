import React, { Component } from 'react';

// styles
import '../../styles/components/Navigation.scss';

export default class Navigation extends Component
{
    // called when user scrolls the page to update header and navigation animation states
    handleScroll = (event) =>
    {
        const scrollEffectsCutoffWidth = 1350;

        // utility variables
        let navBounds, navTopDistanceFromView, toNavigationOpacity, toTopOpacity, navOpacity;
        
        // if window is too wide for effects the nav is set to fully visible and the function returns
        if(window.innerWidth >= scrollEffectsCutoffWidth)
        {
            this.navigation.style.opacity = '1';

            return;
        }

        // determines if navigation is in view and sets the opacity of the
        // to-navigation button, to-top button, and the navigation based on its proximity
        navBounds = this.navigation.getBoundingClientRect();
        navTopDistanceFromView =  navBounds.top - window.innerHeight;

        navOpacity = (350 - navTopDistanceFromView - 600) / 350;
        navOpacity = (navOpacity < 0.05) ? 0.05 : navOpacity;
        toNavigationOpacity = (350 - navTopDistanceFromView - 500) / 350;
        toNavigationOpacity = 1 - toNavigationOpacity;
        toTopOpacity = -toNavigationOpacity;
        toNavigationOpacity = (toNavigationOpacity < 0) ? 0 : toNavigationOpacity;

        // updates the opacity of the to-navigation button, to-top button, and navigation
        this.navigation.style.opacity = `${navOpacity}`;
        this.toNavigation.style.opacity = `${toNavigationOpacity}`;
        this.toTop.style.opacity = `${toTopOpacity}`;

        // prevents the users from clicking the invisible to-navigation or to-top button when
        // they're hidden
        if(toNavigationOpacity === 0 && (this.toNavigation.style.zIndex !== '-1' || this.toTop.style.zIndex !== '200'))
        {
            this.toNavigation.style.zIndex = '-1';
            this.toTop.style.zIndex = '200';
        }
        else if(toNavigationOpacity > 0 && (this.toNavigation.style.zIndex !== '200' || this.toTop.style.zIndex !== '-1'))
        {
            this.toNavigation.style.zIndex = '200';
            this.toTop.style.zIndex = '-1';
        }
    }

    componentDidMount()
    {
        // stores element references that handlers continually use (performance reasons)
        this.toNavigation = document.querySelector('#to-navigation');
        this.toTop = document.querySelector('#to-top');
        this.navigation = document.querySelector('#navigation');

        // attaches scroll handler to the window to add navigation transition
        // effects
        window.onscroll = this.handleScroll;
        window.onresize = this.handleScroll;
        
        // initializes header and navigation transition states
        this.handleScroll();
    }

    render()
    {
        // the id of the header is set to prevent more than one header
        // per webpage
        return (
            <nav id="navigation">
                <div id="navigation-content">
                    <div id="navigation-links">
                        <a id="link-about" href="/">about</a>
                        <a id="link-projects" href="/projects">projects</a>
                        <a id="link-experience" href="/experience">experience</a>
                        <a id="link-current-work" href="/current-work">current-work</a>
                        <a id="link-education" href="/education">education</a>
                        <a id="link-contact" href="/contact">contact</a>
                    </div>
                </div>
            </nav>
        );
    }
}