import React, { Component } from 'react';
import { Consumer } from '../components/context';

// styles
import '../../styles/components/Project.scss';

// utility functions
import { highlightMarkedText } from '../../utility';

export default class Navigation extends Component
{
    render()
    {
        // the id of the header is set to prevent more than one header
        // per webpage
        return (
            <Consumer>
                { ({ actions }) => 
                (
                    <li className="project" id={`${this.props.id}`} key={this.props.id}>
                        <h4>{ highlightMarkedText(this.props.data.display) }</h4>
                        <img className="project-thumbnail" src={`resources/projects/${this.props.id}.png`} alt="" onClick={actions.handleProjectSelection}></img>
                        <p className="project-indicator scroll-based-indicator">click image for details</p>
                        <div className="project-description">
                            <p className="project-lede">{highlightMarkedText(this.props.data.lede)}</p>
                        </div>
                    </li>
                )}
            </Consumer>
        );
    }
}