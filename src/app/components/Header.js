import React, { Component } from 'react';

// styles
import '../../styles/components/Header.scss';

export default class Header extends Component
{
    // handles when the user clicks the menu open button
    handleMenuClick = () =>
    {
        // gets the navigation and smooth scrolls to the element

        let navigation = document.querySelector('#navigation');
        
        let scrollOptions = 
        {
            behavior: 'smooth',
        };

        navigation.scrollIntoView(scrollOptions);
    }

    // handles when the user clicks the close button for the navigation
    handleNavigationCloseRequest = () =>
    {
        // smooth scrolls back to the top

        let scrollOptions = 
        {
            top: 0,
            left: 0,
            behavior: 'smooth',
        };

        window.scrollTo(scrollOptions);
    }

    render()
    {
        // the id of the header is set to prevent more than one header
        // per webpage
        return (
            <header className="header" id="header"> 
                <img id="to-navigation" src="./resources/menu.svg" alt="" onClick={this.handleMenuClick}/>
                <p id="to-top" onClick={this.handleNavigationCloseRequest}>^</p>
            </header>
        );
    }
}