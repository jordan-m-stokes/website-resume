import React from 'react';

import { Component } from 'react';

// styles
import '../../styles/components/Footer.scss';

export default class Footer extends Component
{
    // the id of the footer is set to prevent more than one footer
    // per webpage
    render()
    {
        return (
            <section className="footer" id="footer">
                <p>
                    all icons created by <a href="https://www.flaticon.com/authors/those-icons">Those Icons</a> from
                    <a href="https://www.flaticon.com/" title="Flaticon"> flaticon.com</a>
                </p>
            </section>
        );
    }
}