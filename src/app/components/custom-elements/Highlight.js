import { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';

// these are defined here so that 'utility.js' can use it in its functions
export const openingTag = `<span class="highlight">`;
export const closingTag = `</span>`;

export default class Highlight extends Component
{
    render()
	{
        return ReactHtmlParser(`${openingTag}${this.props.text}${closingTag}`);
    }
}