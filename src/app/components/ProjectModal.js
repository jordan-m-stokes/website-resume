import React, { Component } from 'react';
import {Consumer} from './context';

// styles
import '../../styles/components/ProjectModal.scss';

// utility functions
import { highlight, highlightMarkedText } from '../../utility';

export default class ProjectModal extends Component
{

    state = {
        imageToView: -1,
    }

    // handles when a user clicks on an image in the gallery to view
    handleImageSelection = (event) =>
    {
        let target = event.target;

        // loops back through all previous siblings determining the index of the clicked image

        let cursor = target;
        let i = 0;

        while((cursor = cursor.previousSibling) !== null) 
        {
            i++;
        }

        // sets state to view clicked image

        let newState = 
        {
            imageToView: i,
        };

        this.setState(previousState => newState);
    }

    // handles when the user clicks to close an expanded image being viewed
    handleImageCloseRequest = () =>
    {
        // sets state to view no image returning it to the default project view

        let newState =
        {
            imageToView: -1,
        };

        let lastSelectedImage = this.state.imageToView;

        this.setState(previousState => newState);

        // scrolls down to the previously viewed image

        function revertScroll()
        {
            let modalImages = document.querySelectorAll('.modal-image');

            modalImages[lastSelectedImage].scrollIntoView({ behavior: 'smooth', block: 'center', });
        }

        setTimeout(revertScroll, 100);
    }

    // takes the pointer array from projects.json and makes the jsx for li elements
    buildPointers(pointerData)
    {
        let jsx = [];
        let i = 0;

        for(let pointerText of pointerData)
        {
            let li = <li key={i++} className="modal-pointer">{highlightMarkedText(pointerText)}</li>;

            jsx.push(li);
        }

        return  <ul className="modal-pointers">
                    <h2 key="-1" className="modal-pointers-header">{highlight('f')}eature{highlight('s')}</h2>
                    {jsx}
                </ul>;
    }

    // takes the link array from projects.json and creates the jsx if urls aren't left blank
    buildLinks(linkData)
    {
        let jsx = [];

        if(linkData.liveApp)
        {
            let a = <a key="0" className="link-live-app" rel="noopener noreferrer" href={linkData.liveApp} target="_blank">live app</a>;

            jsx.push(a);
        }
        if(linkData.repository)
        {
            let a = <a key="1" className="link-repository" rel="noopener noreferrer" href={linkData.repository} target="_blank">repository</a>;

            jsx.push(a);
        }

        if(jsx.length > 0)
        {
            return <div className="modal-links">{jsx}</div>;
        }
    }

    // takes the project data from projects.json and makes the jsx for the image elements
    buildImages(projectData)
    {
        let jsx = [];

        for(let i = 0; i < projectData.imageCount; i++)
        {
            let imageUrl = `/resources/projects/${projectData.id}_${i}.png`;
            let img = <img  key={i} value={i} className="modal-image" src={imageUrl} alt="" onClick={this.handleImageSelection}/>;

            jsx.push(img);
        }

        if(jsx.length > 0)
        {
            return <div className="modal-images">{jsx}</div>;
        }
    }

    render()
    {
        // if there is no image to view then the default view is returned
        if(this.state.imageToView === -1)
        {
            return (
                <Consumer>
                    { ({ selectedProjectData, actions }) => 
                    (
                        <div id="project-modal" className="modal hidden">
                            <div className="modal-exit" onClick={actions.handleProjectModalCloseRequest}>x</div>
                            <div className="modal-main-image" style={{backgroundImage: `url("/resources/projects/${selectedProjectData.id}.png")`}}></div>
                            <div className="modal-content">
                                <h2 className="modal-header">{highlightMarkedText(selectedProjectData.display)}</h2>
                                {this.buildLinks(selectedProjectData.links)}
                                <p className="modal-lede">{highlightMarkedText(selectedProjectData.lede)}</p>
                                {this.buildImages(selectedProjectData)}
                                {this.buildPointers(selectedProjectData.pointers)}
                            </div>
                        </div>
                    )}
                </Consumer>
            );
        }
        // if there is, the image is displayed
        else
        {
            return ( 
                <Consumer>
                    { ({ selectedProjectData, actions }) => 
                    (
                        <div id="project-modal" className="modal hidden">
                            <div className="modal-exit" onClick={actions.handleProjectModalCloseRequest}>x</div>
                            <div className="modal-main-image" style={{backgroundImage: `url("/resources/projects/${selectedProjectData.id}.png")`}}></div>
                            <div className="modal-content">
                                <div className="modal-exit image-close" onClick={this.handleImageCloseRequest}>x</div>
                                <img className="modal-image-to-view" src={`resources/projects/${selectedProjectData.id}_${this.state.imageToView}.png`} alt=""></img>
                            </div>
                        </div>
                    )}
                </Consumer>
            );
        }
    }
}